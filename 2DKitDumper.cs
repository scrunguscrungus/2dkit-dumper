﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace _2DKitDumper
{
	class Program
	{
		static string[] commonFiles = { "xml/config.xml", "flambe.js", "strings/strings.xml", "index.html", "targets/main-html.js" };
		static void Main( string[] args )
		{
			Console.Write( "Base URL: " );
			string baseURL = Console.ReadLine();

			Console.Write( "assets JSON: " );
			string assetsJSONFile = Console.ReadLine();

			Console.WriteLine( "Output location: " );
			string outLoc = Console.ReadLine();

			string assets = File.ReadAllText( assetsJSONFile );

			JObject assetsObj = JObject.Parse( assets );

			using ( WebClient wc = new WebClient() )
			{
				Console.WriteLine( "Performing preliminary dump of common files" );

				foreach ( string commonFile in commonFiles )
				{
					string fileURL = $"{baseURL}/{commonFile}";

					string targetFile = Path.Combine( outLoc, $"{commonFile}" );

					if ( !Directory.Exists( Path.GetDirectoryName( targetFile ) ) )
						Directory.CreateDirectory( Path.GetDirectoryName( targetFile ) );

					Console.WriteLine( $"Downloading {fileURL} to {targetFile}" );

					try
					{
						wc.DownloadFile( fileURL, targetFile );
						Console.WriteLine( "Complete!" );
					}
					catch ( Exception e )
					{
						Console.WriteLine( "Failed: {0}", e.Message );
					}
				}

				foreach ( JToken x in assetsObj[ "assets" ] )
				{
					JProperty prp = (JProperty)x;
					JArray files = (JArray)prp.Value;
					Console.WriteLine( "Processing scene: {0}", prp.Name );
					foreach ( JObject file in files )
					{
						JToken filename = file[ "name" ];

						string fileURL = $"{baseURL}/assets/{prp.Name}/{filename}";

						string targetFile = Path.Combine( outLoc, $"assets/{prp.Name}/{filename}" );

						if ( !Directory.Exists( Path.GetDirectoryName( targetFile ) ) )
							Directory.CreateDirectory( Path.GetDirectoryName( targetFile ) );

						Console.WriteLine( $"Downloading {fileURL} to {targetFile}" );

						try
						{
							wc.DownloadFile( fileURL, targetFile );
							Console.WriteLine( "Complete!" );
						}
						catch ( Exception e )
						{
							Console.WriteLine( "Failed: {0}", e.Message );
						}
					}
				}
			}
			Console.WriteLine( "All files dumped!" );
			Console.ReadKey();
		}
	}
}
